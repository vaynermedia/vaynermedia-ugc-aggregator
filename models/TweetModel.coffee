mongoose = require 'mongoose'


TweetSchema = 
	tweetId:
		type: String
	text: 
		type: String
	user_id:
		type: String
	user_screen_name:
		type: String
	user_profile_image_url:
		type: String
	urls:
		type: String
	vine_url:
		type: String

TweetModel = mongoose.model 'TweetModel', TweetSchema