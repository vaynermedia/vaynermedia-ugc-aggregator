require('coffee-script')

express         = require('express')
mongodb     	= require 'mongodb'

# globals
global._        = require 'underscore'
global.app      = express()
global.log 		= console.log

require '../config'

#app.get '/', (req, res) ->
db = app.get 'database_config'

app.get '/', (req, res) ->
	mongodb.Db.connect db.uri, (error, client) ->
		throw error if error

		res.writeHead(200, 'Content-Type': 'application/json' )

		skip = req.query['skip'] || 0
		limit = req.query['limit'] || 6

		collection = new mongodb.Collection(client, 'tweet')
		collection.find {'approved' : 1}, {sort: [['_id', 'desc']], skip: skip, limit: limit}, (err, cursor) ->
			cursor.toArray (err, docs) ->
				res.write( JSON.stringify docs )
				client.close()
				res.end()

# health check!
app.get '/check', (req, res) ->
    res.send('ok!')

app.listen 3000
log 'Listening on port 3000'

