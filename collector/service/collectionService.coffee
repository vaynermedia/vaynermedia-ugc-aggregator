#
# Manages collected tweets
#
Backbone    = require 'backbone'
mongodb     = require 'mongodb'
url         = require 'url'
log         = console.log


#
# export a collection service
#
module.exports = CollectionService = Backbone.Model.extend

    defaults:
        conn: ''

    # init!
    initialize: () ->
        db  = app.get 'database_config'
        @set('conn',  db.uri)


    saveTweet: (model) ->
        mongodb.Db.connect @get('conn'), (error, client) =>
            throw error if error
            
            collection = new mongodb.Collection(client, 'tweet')

            collection.insert
                tweetId: model.get('tweetId')
                text: model.get('text')
                user_id: model.get('user_id')
                user_name: model.get('user_screen_name')
                user_profile_image_url: model.get('user_profile_image_url')
                urls: model.get('urls')
                vine_url: model.get('vine_url')
                approved: model.get('approved')
                , (err, objects) ->
                    if err
                        console.warn err.message

            client.close()
            null