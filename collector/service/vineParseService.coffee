# ParseService

Backbone    = require 'backbone'
log         = console.log


#
# export a collection service
#
module.exports = VineParseService = Backbone.Model.extend

    defaults:
        conn: ''

    # init!
    initialize: () ->

    #
    # Parse a tweet to see if it contains a vine
    parse: (tweetModel) ->
    	isVine = false

    	_.each tweetModel.get('urls'), (url) ->
            if url.indexOf('vine.co') > -1
                tweetModel.set('vine_url', url)
                isVine = true
    	isVine


