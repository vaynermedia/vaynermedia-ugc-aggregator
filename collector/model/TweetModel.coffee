# TweetModel
Backbone            = require 'backbone'

module.exports = TweetModel = Backbone.Model.extend
    
	# define some defaults
	# defaults:
	# 	tweetId: 0
	# 	text: ''
	# 	user_id: ''
	# 	user_screen_name: ''
	# 	profile_image_url: ''
	# 	created_at: ''
	# 	urls: []
	
	
	loadFromTweet: (data) ->
		@set('tweetId', data.id)
		@set('text', data.text)
		@set('user_id', data.user.id)
		@set('user_screen_name', data.user.screen_name)
		@set('user_profile_image_url', data.user.profile_image_url)
		@set('vine_url', '')
		@set('approved', 0)

		urls = []
		_.each data.entities.urls, (url) ->
	      urls.push url.expanded_url
	    	
	   	@set('urls', urls)
	   	@



