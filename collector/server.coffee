require('coffee-script')

express         = require('express')

# globals
global._        = require 'underscore'
global.app      = express()

# auto-load all configurations
require '../config'

# require some custom classes
CollectionService = require './service/collectionService'
VineParseService  = require './service/vineParseService'
TweetModel        = require './model/TweetModel'

service = new CollectionService
parser = new VineParseService

# hashtag to track
#track = 'vine'
track = 'girlsunstoppable'

# let's start playing with zee twitter
twitter = require 'ntwitter'
twitter_config = app.get 'twitter_config'

twit = new twitter
  consumer_key: twitter_config.consumer_key
  consumer_secret: twitter_config.consumer_secret
  access_token_key: twitter_config.access_token_key
  access_token_secret: twitter_config.access_token_secret

console.log 'starting server!'

# now let's streeeeaaamm!
twit.stream 'statuses/filter', 'track' : track, (stream) ->
  stream.on 'data', (data) ->
    # console.log ''
    # console.log '----------------------------------'
    # console.log 'tweet id: ' + data.id
    # console.log 'tweet text: ' + data.text
    # console.log 'tweet user id: ' + data.user.id
    # console.log 'tweet user name: ' + data.user.screen_name
    # console.log 'tweet urls: '
    # _.each data.entities.urls, (url) ->
    #     console.log url.expanded_url
    
    # console.log ''
    # console.log '----------------------------------'
    # console.log 'tweet id: ' + data.id
    
    tweetModel = new TweetModel().loadFromTweet(data)

    if parser.parse tweetModel
      console.log 'adding ' + tweetModel.get('vine_url')
      service.saveTweet tweetModel


