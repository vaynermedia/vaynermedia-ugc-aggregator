
/*
 * GET approve listing.
 */

exports.approve = function(req, res){
  res.render('approve'
  	, {
  		title: 'Approve Vines'
  		, tweets: ['first', 'second']
  	}
  );
};
