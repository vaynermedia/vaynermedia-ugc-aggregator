mongodb     	= require 'mongodb'
ObjectId 		= mongodb.ObjectID


#
# GET approve listing
#


module.exports.list = (req, res) ->
	mongodb.Db.connect db_uri, (error, client) ->
		throw error if error

		collection = new mongodb.Collection(client, 'tweet')
		collection.find {'approved' : 0}, {limit: 1}, (err, cursor) ->
			cursor.toArray (err, docs) ->
				res.render 'approve',
					title: 'Approve Vines'
					tweets: docs
				client.close()

# approve a tweet
module.exports.approve = (req, res) ->
	mongodb.Db.connect db_uri, (error, client) ->
		throw error if error

		collection = new mongodb.Collection(client, 'tweet')
		collection.update {'_id' : new ObjectId(req.params.id) }, {$set: {'approved' : 1} }, () ->

			collection.find {'approved' : 0}, {limit: 1}, (err, cursor) ->
				cursor.toArray (err, docs) ->
					res.render 'approve',
						title: 'Approve Vines'
						tweets: docs
					client.close()


# deny a tweet
module.exports.deny = (req, res) ->
	mongodb.Db.connect db_uri, (error, client) ->
		throw error if error

		collection = new mongodb.Collection(client, 'tweet')
		collection.update {'_id' : new ObjectId(req.params.id) }, {$set: {'approved' : -1} }, () ->

			collection.find {'approved' : 0}, {limit: 1}, (err, cursor) ->
				cursor.toArray (err, docs) ->
					res.render 'approve',
						title: 'Approve Vines'
						tweets: docs
					client.close()


		
