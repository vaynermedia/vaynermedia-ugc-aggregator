global._        = require 'underscore'
express         = require 'express'
global.app      = express()
#global.app.set('env', 'dev')

require '../config'

mongoose 		= require 'mongoose'
routes 			= require './routes'
approve 		= require './routes/approve'
http 			= require 'http'
path 			= require 'path'

global.db_uri	= app.get('database_config').uri


# express yourself
app.set('port', process.env.PORT || 3000)
app.set('views', __dirname + '/views')
app.set('view engine', 'jade')
app.use(express.favicon())
app.use(express.logger('dev'))
app.use(express.bodyParser())
app.use(express.methodOverride())
app.use(express.cookieParser('cookiemonster'))
app.use(express.session())
app.use(app.router)
app.use(express.static(path.join(__dirname, 'public')))

if ('dev' == app.get('env'))
  app.use(express.errorHandler())

# routing
app.get('/', routes.index)
app.get('/list', approve.list)
app.get('/approve/:id', approve.approve)
app.get('/deny/:id', approve.deny)

app.set('port', '3030') # ensure separate port for admin
http.createServer(app).listen app.get('port'), () ->
  console.log('Express server listening on port ' + app.get('port'))





